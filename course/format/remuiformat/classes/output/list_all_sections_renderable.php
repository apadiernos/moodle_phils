<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sinigle Section Renderable - A topics based format that uses card layout to diaply the content.
 *
 * @package course/format
 * @subpackage remuiformat
 * @copyright  2019 WisdmLabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace format_remuiformat\output;
defined('MOODLE_INTERNAL') || die();

use renderable;
use renderer_base;
use templatable;
use stdClass;
use context_course;
use html_writer;
use moodle_url;
use core_completion\progress;
use core_course\external\course_summary_exporter;

/**customized by alan**/
use user_picture;
use course_enrolment_manager;
require_once($CFG->dirroot.'/enrol/locallib.php');

require_once($CFG->dirroot.'/course/format/renderer.php');
require_once($CFG->dirroot.'/course/renderer.php');
require_once($CFG->dirroot.'/course/format/remuiformat/classes/mod_stats.php');
require_once($CFG->dirroot.'/course/format/remuiformat/classes/course_format_data_common_trait.php');
require_once($CFG->dirroot.'/course/format/remuiformat/lib.php');

/**
 * This file contains the definition for the renderable classes for the card all sections summary page.
 *
 * @package   format_remuiformat
 * @copyright  2019 WisdmLabs
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_remuiformat_list_all_sections implements renderable, templatable {

    // Variables.
    private $course;
    private $courseformat;
    protected $courserenderer;
    private $modstats;
    private $courseformatdatacommontrait;
    private $settings;

    /**
     * Constructor
     */
    public function __construct($course, $renderer) {
        $this->courseformat = course_get_format($course);
        $this->course = $this->courseformat->get_course();
        $this->courserenderer = $renderer;
        $this->modstats = \format_remuiformat\ModStats::getinstance();
        $this->courseformatdatacommontrait = \format_remuiformat\course_format_data_common_trait::getinstance();
        $this->settings = $this->courseformat->get_settings();
    }

    /**
     * Function to export the renderer data in a format that is suitable for a
     * question mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return stdClass|array
     */
    public function export_for_template(renderer_base $output) {
        global $PAGE, $CFG;
        unset($output);
        $export = new \stdClass();
        $renderer = $PAGE->get_renderer('format_remuiformat');
        $rformat = $this->settings['remuicourseformat'];

        // Get necessary default values required to display the UI.
        $editing = $PAGE->user_is_editing();
        $export->editing = $editing;
        $export->courseformat = get_config('format_remuiformat', 'defaultcourseformat');
        
		/**customized by alan**/
		if (!$editing) {
			$PAGE->requires->js_call_amd('format_remuiformat/custom', 'init');
		}
		if ($rformat == REMUI_LIST_FORMAT) {
            $PAGE->requires->js_call_amd('format_remuiformat/format_list', 'init');
            $this->get_list_format_context($export, $renderer, $editing, $rformat);
        }

        return  $export;
    }

    private function get_list_format_context(&$export, $renderer, $editing, $rformat) {
        global $DB, $OUTPUT, $USER;
        $coursecontext = context_course::instance($this->course->id);
        $modinfo = get_fast_modinfo($this->course);

        // Default view for all sections.
        $defaultview = $this->settings['remuidefaultsectionview'];
        $export->defaultview = $defaultview;
        if ($defaultview == 1) {
            $export->expanded = true;
            $export->collapsed = false;
        } else {
            $export->collapsed = true;
            $export->expanded = false;
        }
        // User id for toggle.
        $export->user_id = $USER->id;
        // Course Information.
        $export->course_id = $this->course->id;
        $imgurl = $this->courseformatdatacommontrait->display_file($this->settings['remuicourseimage_filemanager']);
        // General Section Details.
        $generalsection = $modinfo->get_section_info(0);
        if ($editing) {
            $export->generalsection['generalsectiontitlename'] = $this->courseformat->get_section_name($generalsection);
            $export->generalsection['generalsectiontitle'] = $renderer->section_title($generalsection, $this->course);
        } else {
            $export->generalsection['generalsectiontitle'] = $this->courseformat->get_section_name($generalsection);
        }
        $generalsectionsummary = $renderer->format_summary_text($generalsection);
        $export->generalsectionsummary = $generalsectionsummary;
        $export->generalsection['remuicourseimage'] = $imgurl;
        // For Completion percentage.
        $export->generalsection['activities'] = $this->courseformatdatacommontrait->get_list_activities_details(
            $generalsection,
            $this->course,
            $this->courserenderer,
            $this->settings
        );

        $completion = new \completion_info($this->course);
        $percentage = progress::get_course_progress_percentage($this->course);
        if (!is_null($percentage)) {
            $percentage = floor($percentage);
            $export->generalsection['percentage'] = $percentage;
        }
		/**customized by alan**/
		$export->custom = $this->custom_fields();

        // For right side.
        $rightside = $renderer->section_right_content($generalsection, $this->course, false);
        $export->generalsection['rightside'] = $rightside;
        $displayteacher = $this->settings['remuiteacherdisplay'];
        if ($displayteacher == 1) {
            $role = $DB->get_record('role', array('shortname' => 'editingteacher'));
            $teachers = null;
            if (!empty($role)) {
                $teachers = get_role_users($role->id, $coursecontext);
            }
            // For displaying teachers.
            if (!empty($teachers)) {
                $count = 1;
                $export->generalsection['teachers'] = $teachers;
                $export->generalsection['teachers']['teacherimg'] = '
                <div class="teacher-label"><span>'
                .get_string('teachers', 'format_remuiformat').
                '</span></div>
                <div class="carousel slide" data-ride="carousel" id="teachers-carousel">
                <div class="carousel-inner text-center">';

                foreach ($teachers as $teacher) {
                    if ($count % 2 == 0) {
                        // Skip even members.
                        $count += 1;
                        next($teachers);
                        continue;
                    }
                    $teacher->imagealt = $teacher->firstname . ' ' . $teacher->lastname;
                    if ($count == 1) {
                        $export->generalsection['teachers']['teacherimg'] .=
                        '<div class="carousel-item active"><div class="teacher-img-container">' . $OUTPUT->user_picture($teacher);

                    } else {
                        $export->generalsection['teachers']['teacherimg'] .=
                        '<div class="carousel-item"><div class="teacher-img-container">'. $OUTPUT->user_picture($teacher);
                    }
                    $nextteacher = next($teachers);
                    if (false != $nextteacher) {
                        $nextteacher->imagealt = $nextteacher->firstname . ' ' . $nextteacher->lastname;
                        $export->generalsection['teachers']['teacherimg'] .= $OUTPUT->user_picture($nextteacher);
                    }
                    $export->generalsection['teachers']['teacherimg'] .= '</div></div>';
                    $count += 1;
                }
                if (count($teachers) > 2) {
                    $export->generalsection['teachers']['teacherimg'] .=
                    '</div><a class="carousel-control-prev" href="#teachers-carousel" role="button" data-slide="prev">
                            <i class="fa fa-chevron-left"></i>
                            <span class="sr-only">'
                            .get_string('previous', 'format_remuiformat').
                            '</span>
                        </a>
                        <a class="carousel-control-next" href="#teachers-carousel" role="button" data-slide="next">
                            <i class="fa fa-chevron-right"></i>
                            <span class="sr-only">'
                            .get_string('next', 'format_remuiformat').
                            '</span>
                        </a></div>';
                } else {
                    $export->generalsection['teachers']['teacherimg'] .= '</div></div>';
                }
            }
        }
        // Add new activity.
        $export->generalsection['addnewactivity'] = $this->courserenderer->course_section_add_cm_control($this->course, 0, 0);
        // Setting up data for remianing sections.
		$export->sections = $this->courseformatdatacommontrait->get_all_section_data(
            $renderer,
            $editing,
            $rformat,
            $this->settings,
            $this->course,
            $this->courseformat,
            $this->courserenderer
        );
    }
	
	/**customized by alan**/	
	private function custom_fields()
	{
		global $DB, $PAGE, $CFG;
		$coursecontext = context_course::instance($this->course->id);
		$completion = new \completion_info($this->course);
		$rolestudent = $DB->get_record('role', array('shortname' => 'student'));
		$manager = new \course_enrolment_manager($PAGE, $this->course,null,$rolestudent->id);
		$userenrolments = $manager->get_total_users();
		// Get the number of modules that support completion.
        $modules = $completion->get_activities();
        $count = count($modules);
        // Get the number of modules that have been completed.
        $completed = 0;
        foreach ($modules as $module) {
            $data = $completion->get_data($module, true, $userid);
			if($module->modname == 'bigbluebuttonbn')
				$bigbluebuttonbn = $module;
			
            $completed += $data->completionstate == COMPLETION_INCOMPLETE ? 0 : 1;
        }	
        $percentage = progress::get_course_progress_percentage($this->course);
        if (!is_null($percentage)) {
            $percentage = floor($percentage);
        }
		if($bigbluebuttonbn)
			$bbb = $this->init_bbb($bigbluebuttonbn);
		
		
		
		$role = $DB->get_record('role', array('shortname' => 'editingteacher'));	
		$teachers = get_role_users($role->id, $coursecontext);
		$teacherData = array();
		$key = 0;
		foreach($teachers as $teacher){
			$teacherRec = $DB->get_record('user', array('id' => $teacher->id));
			$teacherData[$key] = array(
								'name' => $teacher->firstname . ' ' . $teacher->lastname,
								'url'  => $CFG->wwwroot.'/user/profile.php?id='.$teacher->id,
                                'picture' => self::get_user_picture($teacher),
                                'description' => $teacherRec->description,
							);
			$key++;
		}
		if( $this->course->enddate  ){
			$compute_duration = round(abs($this->course->enddate - time())/60/60/24);
			$duration =  $compute_duration > 1 ? $compute_duration.' days' : $compute_duration.' day';
		}
		return [
			'course'=>$this->course,
			'bbb'=>$bbb,
			'enrolled'=>$userenrolments > 1 ? $userenrolments.' Students' : $userenrolments.' Student',
			'items'=>$completed.' of '.$count,
			'teachers'=>$teacherData,
			'progress_status'=>$percentage == 100 ? 1 : 0,
			'percentage'=>$percentage ? $percentage : 0,
			'remaining_date'=>$this->course->enddate ? $duration : null,
			'deadline'=>$this->course->enddate ? format_datetime($this->course->enddate) : null,
		];		
	}
	/**customized by alan**/
	private function init_bbb($cm)
	{
		global $PAGE, $CFG, $SESSION;
		require_once($CFG->dirroot.'/mod/bigbluebuttonbn/locallib.php');
		require_once($CFG->dirroot.'/mod/bigbluebuttonbn/viewlib.php');

		$viewinstance = bigbluebuttonbn_view_validator($cm->id, 0); // In locallib.
		if (!$viewinstance) {
			print_error('view_error_url_missing_parameters', plugin::COMPONENT);
		}

		$cm = $viewinstance['cm'];
		$course = $viewinstance['course'];
		$bigbluebuttonbn = $viewinstance['bigbluebuttonbn'];

		require_login($course, true, $cm);

		// In locallib.
		bigbluebuttonbn_event_log(\mod_bigbluebuttonbn\event\events::$events['view'], $bigbluebuttonbn);

		// Additional info related to the course.
		$bbbsession['course'] = $course;
		$bbbsession['coursename'] = $course->fullname;
		$bbbsession['cm'] = $cm;
		$bbbsession['bigbluebuttonbn'] = $bigbluebuttonbn;
		// In locallib.
		bigbluebuttonbn_view_bbbsession_set($PAGE->context, $bbbsession);		
		$type = null;
		if (isset($bbbsession['bigbluebuttonbn']->type)) {
			$type = $bbbsession['bigbluebuttonbn']->type;
		}
		$typeprofiles = bigbluebuttonbn_get_instance_type_profiles();
		$enabledfeatures = bigbluebuttonbn_get_enabled_features($typeprofiles, $type);
		$pinginterval = (int)\mod_bigbluebuttonbn\locallib\config::get('waitformoderator_ping_interval') * 1000;
		$PAGE->requires->strings_for_js(array_keys(bigbluebuttonbn_get_strings_for_js()), 'bigbluebuttonbn');
		$output = '';
		$activity = bigbluebuttonbn_view_session_config($bbbsession, $cm->id);
		$jsvars = array('activity' => $activity, 'ping_interval' => $pinginterval,
			'locale' => bigbluebuttonbn_get_localcode(), 'profile_features' => $typeprofiles[0]['features']);			
		if ($enabledfeatures['showroom']) {
			$output .= bigbluebuttonbn_view_render_room2($bbbsession, $activity, $jsvars);
			$PAGE->requires->yui_module('moodle-mod_bigbluebuttonbn-rooms','M.mod_bigbluebuttonbn.rooms.init', array($jsvars));
			$PAGE->requires->yui_module('moodle-mod_bigbluebuttonbn-broker', 'M.mod_bigbluebuttonbn.broker.init', array($jsvars));
			$SESSION->bigbluebuttonbn_bbbsession = $bbbsession;
			return $output;
		}		
		return;				
	}
    /**customized by alan**/
    public static function get_user_picture($userobject = null, $imgsize = 400) {
        global $USER, $PAGE;
		if (!$userobject) {
            $userobject = $USER;
        }
        $userimg = new user_picture($userobject);
        $userimg->size = $imgsize;
        return  $userimg->get_url($PAGE);
    }	
}
/**customized by alan**/
function bigbluebuttonbn_view_render_room2(&$bbbsession, $activity, &$jsvars) {
    // JavaScript variables for room.
    $openingtime = '';
    if ($bbbsession['openingtime']) {
        $openingtime = get_string('mod_form_field_openingtime', 'bigbluebuttonbn').': '.
            userdate($bbbsession['openingtime']);
    }
    $closingtime = '';
    if ($bbbsession['closingtime']) {
        $closingtime = get_string('mod_form_field_closingtime', 'bigbluebuttonbn').': '.
            userdate($bbbsession['closingtime']);
    }
    $jsvars += array(
        'meetingid' => $bbbsession['meetingid'],
        'bigbluebuttonbnid' => $bbbsession['bigbluebuttonbn']->id,
        'userlimit' => $bbbsession['userlimit'],
        'opening' => $openingtime,
        'closing' => $closingtime,
    );
    $output = '<br><div class="text-muted text-center" id="status_bar"></div>';
    $output .= '<br><span id="control_panel" class="hidden"></span>';
    // Action button box.
    $output .= '<div class="mt-1 w-full btn-bbb" id="join_button" ></div><div class="mt-1 w-full btn-bbb" id="end_button"></div>'."\n";

    if ($activity == 'ended') {
        $output .= bigbluebuttonbn_view_ended($bbbsession);
    }
    return $output;
}