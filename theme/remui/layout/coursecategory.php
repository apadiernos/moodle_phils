
<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A two column layout for the Edwiser RemUI theme.
 *
 * @package   theme_remui
 * @copyright WisdmLabs
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $PAGE, $USER, $SITE, $COURSE;

require_once('common.php');

// prepare course archive context
$hascourses = false;
$mycourses = optional_param('mycourses', 0, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$category = optional_param('categoryid', 0, PARAM_INT);
$page = optional_param('page', 0, PARAM_INT);
$pageurl = new moodle_url('/course/index.php');

if (!empty($search)) {
    $pageurl->param('search', $search);
}
if (!empty($category)) {
    $pageurl->param('categoryid', $category);
}
if (!empty($mycourses)) {
    $pageurl->param('mycourses', $mycourses);
}


$courseperpage =  \theme_remui\toolbox::get_setting('courseperpage');
if (empty($courseperpage)) {
    $courseperpage = 12;
}

$startfrom  = $page * $courseperpage;
$courses    = \theme_remui\utility::get_courses(false, $search, $category, $startfrom, $courseperpage, $mycourses);
$totalcourses = \theme_remui\utility::get_courses(true, $search, $category, 0, 0, $mycourses);
$totalpages = ceil($totalcourses / $courseperpage);
$pagingbar  = new paging_bar($totalcourses, $page, $courseperpage, $pageurl, 'page');
if(count($courses) > 0) {
    $hascourses = true;
}

$templatecontext['hascourses'] = $hascourses;
$templatecontext['courses'] = $courses;
$templatecontext['categoryfilter'] = \theme_remui\utility::get_course_category_selector($category, $search, $mycourses, $pageurl);
$templatecontext['searchfilter'] = $PAGE->get_renderer('core', 'course')->course_search_form($search, '', $category, $mycourses);
$templatecontext['pagination'] = $OUTPUT->render($pagingbar);
$templatecontext['mycourses'] = $mycourses;
echo $OUTPUT->render_from_template('theme_remui/coursecategory', $templatecontext);

