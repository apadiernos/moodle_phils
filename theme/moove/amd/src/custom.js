define(['jquery','theme_boost/popover'], function($,popover) {
    'use strict';
	
	
    return {
        init: function() {
			$('body')
			  .on('mousedown', '.popover-alan', function(e) {
				console.log("clicked inside popover")
				e.preventDefault()
			  });			
			$('li.list-group-item-action[data-key="sections"]').click(function(){
				var curriculum_pane = $('.course-page-alan').find('.tab-pane#curriculum');
				var curriculum_link = $('.course-page-alan li.nav-item').find('a.nav-link[href="#curriculum"]');
				if( !curriculum_pane.hasClass('active') ){
					$('.course-page-alan li.nav-item').removeClass('active');
					curriculum_link.click();
				}
			});
			$('.custom-popover[data-toggle="popover"]').each(function(i, obj) {
				var popover_target = $(this).attr('data-popover-target');
				var popover_style = $(this).attr('data-popover-style');
				var popover_trigger = $(this).attr('data-trigger') ? $(this).attr('data-trigger') : 'focus';
				$(this).popover({
					html: true,
					trigger: popover_trigger,
					placement: 'top',
					template: '<div class="popover '+popover_style+' popover-alan" role="tooltip"><div class="arrow"></div><div class="popover-body p-0"></div></div>',
					content: function(obj) {
						return $(popover_target).html();
					}
				});
			  });	
			  
			var last_width = $('.search-filters').width();  
			$(window).resize(function(){
				resize_list(0);
			})	
			$('.course-category-buttons a').click(function(){
				$('.card-footer [data-toggle="popover"]').popover('hide');
				$('.course-category-buttons a').attr('class','btn btn-outline-primary');
				$(this).attr('class','btn btn-primary');
				if( $(this).attr('view') == 1 ){
					grid_view();
				}else{
					resize_list(1);	
				}
			})
			function resize_list(ret)
			{
				var resized = 0;
				var current_width = $('.search-filters').width();
				if( ret == 1 && current_width > 650 ){
					list_view();
				}else{
					if( last_width != current_width && current_width <= 650 ){
						grid_view();
						resized = 1;
					}
				}	
				last_width = current_width;
				return resized;
			}
			function grid_view()
			{
				$('.course-category-main .card-container .card').removeClass('h-200').removeClass('row');
				$('.course-category-main .card-container').removeClass('list-view');				
				if( $('.course-category-main .card-container.grid-view').length )
					return;
				$('.course-category-main .card-container').attr('class','col-12 col-sm-6 col-md-4 col-lg-3 d-sm-flex card-container grid-view');
				$('.course-category-main .teacher-block').attr('style','position: absolute; top: 175px; right: 25px; left: auto; z-index: 1');
				$('.course-category-main .btn-status').attr('style','');
				$('.card-dynamic1').attr('class','card-dynamic1');
				$('.card-dynamic2').attr('class','card-dynamic2');			
			}			
			function list_view()
			{
				$('.course-category-main .card-container').removeClass('grid-view');
				if( $('.course-category-main .card-container.list-view').length )
					return;					
				$('.course-category-main .card-container').attr('class','col-12 col-sm-12 col-md-12 col-lg-12 d-sm-flex card-container list-view');
				$('.course-category-main .card-container .card').addClass('h-200').addClass('row');
				$('.course-category-main .teacher-block').attr('style','position: absolute; top: 60px; left: -46px; right: auto; z-index: 1');
				$('.course-category-main .btn-status').attr('style','position: absolute;bottom: -50px;width: 100%;');
				$('.card-dynamic1').attr('class','col-12 col-md-8 col-lg-3 card-dynamic1');
				$('.card-dynamic2').attr('class','col-12 col-md-8 col-lg-9 card-dynamic2');				
			}
        }
    };	

});	