<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A two column layout for the moove theme.
 *
 * @package   theme_moove
 * @copyright 2017 Willian Mano - http://conecti.me
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
use theme_remui;

defined('MOODLE_INTERNAL') || die();

user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
user_preference_allow_ajax_update('sidepre-open', PARAM_ALPHA);

require_once($CFG->libdir . '/behat/lib.php');

$hasdrawertoggle = false;
$navdraweropen = false;
$draweropenright = false;

if (isloggedin()) {
    $hasdrawertoggle = true;
    $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
    $draweropenright = (get_user_preferences('sidepre-open', 'true') == 'true');
}

$blockshtml = $OUTPUT->blocks('side-pre');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;

$extraclasses = [];
if ($navdraweropen) {
    $extraclasses[] = 'drawer-open-left';
}

if ($draweropenright && $hasblocks) {
    $extraclasses[] = 'drawer-open-right';
}

$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();
$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'hasblocks' => $hasblocks,
    'bodyattributes' => $bodyattributes,
    'hasdrawertoggle' => $hasdrawertoggle,
    'navdraweropen' => $navdraweropen,
    'draweropenright' => $draweropenright,
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
	'usercanmanage' => \theme_moove\util\utility::check_user_admin_cap(),
];



// prepare course archive context
$hascourses = false;
$mycourses = optional_param('mycourses', 0, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$category = optional_param('categoryid', 0, PARAM_INT);
$page = optional_param('page', 0, PARAM_INT);
$pageurl = new moodle_url('/course/index.php');

if (!empty($search)) {
    $pageurl->param('search', $search);
}
if (!empty($category)) {
    $pageurl->param('categoryid', $category);
}
if (!empty($mycourses)) {
    $pageurl->param('mycourses', $mycourses);
}else{
	if( !$templatecontext['usercanmanage'] )
		$mycourses = 1;
	
}

if (empty($courseperpage)) {
    $courseperpage = 12;
}

$startfrom  = $page * $courseperpage;
$courses    = \theme_moove\util\utility::get_courses(false, $search, $category, $startfrom, $courseperpage, $mycourses);
$totalcourses = \theme_moove\util\utility::get_courses(true, $search, $category, 0, 0, $mycourses);
$totalpages = ceil($totalcourses / $courseperpage);
$pagingbar  = new paging_bar($totalcourses, $page, $courseperpage, $pageurl, 'page');
if(count($courses) > 0) {
    $hascourses = true;
}

$templatecontext['hascourses'] = $hascourses;
$templatecontext['courses'] = $courses;
$templatecontext['categoryfilter'] = \theme_moove\util\utility::get_course_category_selector($category, $search, $mycourses, $pageurl);
$templatecontext['searchfilter'] = $PAGE->get_renderer('core', 'course')->course_search_form($search, '', $category, $mycourses);
$templatecontext['pagination'] = $OUTPUT->render($pagingbar);
$templatecontext['mycourses'] = $mycourses;

// Improve boost navigation.
theme_moove_extend_flat_navigation($PAGE->flatnav);

$templatecontext['flatnavigation'] = $PAGE->flatnav;

$themesettings = new \theme_moove\util\theme_settings();

$templatecontext = array_merge($templatecontext, $themesettings->footer_items());

echo $OUTPUT->render_from_template('theme_moove/customized/_coursecategory', $templatecontext);
